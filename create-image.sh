#!/bin/sh
set -eux
pacman --needed --noconfirm -S multipath-tools cryptsetup qemu-emulators-full
if [ ! -f ubuntu-base.img ]; then
    wget -nc https://cdimage.ubuntu.com/releases/22.04/release/ubuntu-22.04-preinstalled-server-arm64+raspi.img.xz -O ubuntu-base.img.xz
    unxz ubuntu-base.img.xz
fi
cp ubuntu-base.img ubuntu-target.img
if [ ! -f authorized_keys ]; then echo "No authorized_keys file found in $PWD" && exit 1; fi
if [[ -L /dev/mapper/loop0p1 || -L /dev/mapper/loop0p2 || -L /dev/mapper/loop1p1 || -L /dev/mapper/loop1p2 ]]; then echo "Error: Loop device may already be in use" && exit 1; fi
kpartx -ar "$PWD/ubuntu-base.img"
kpartx -a "$PWD/ubuntu-target.img"
mkdir -p /mnt/original
mount /dev/mapper/loop0p2 /mnt/original
cryptsetup luksFormat -q -c xchacha20,aes-adiantum-plain64 --pbkdf-memory 512000 /dev/mapper/loop1p2
cryptsetup open /dev/mapper/loop1p2 crypted
mkfs.ext4 /dev/mapper/crypted
mkdir -p /mnt/chroot/
mount /dev/mapper/crypted /mnt/chroot/
rsync --archive --hard-links --acls --xattrs --one-file-system --numeric-ids --info="progress2" /mnt/original/* /mnt/chroot/
mkdir -p /mnt/chroot/boot/
mount /dev/mapper/loop1p1 /mnt/chroot/boot/
mount -t proc none /mnt/chroot/proc/
mount -t sysfs none /mnt/chroot/sys/
mount -o bind /dev /mnt/chroot/dev/
mount -o bind /dev/pts /mnt/chroot/dev/pts/
mkdir -p /mnt/chroot/etc/dropbear/initramfs/
cp authorized_keys /mnt/chroot/etc/dropbear/initramfs/authorized_keys
chmod 0600 /mnt/chroot/etc/dropbear/initramfs/authorized_keys
cp chroot.sh /mnt/chroot/
cp /usr/bin/qemu-arm-static /mnt/chroot/usr/bin/
systemctl restart systemd-binfmt.service
LANG=C chroot /mnt/chroot/ /chroot.sh
