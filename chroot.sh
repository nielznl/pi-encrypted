#!/bin/sh
set -eux
if [ ! -f /etc/resolv.conf.bak ]; then
    mv /etc/resolv.conf /etc/resolv.conf.bak
fi
echo 'nameserver 1.1.1.1' > /etc/resolv.conf
apt update
DEBIAN_FRONTEND=noninteractive apt install -y busybox cryptsetup dropbear-initramfs
sed -i 's/LABEL=writable/\/dev\/mapper\/crypted/' /etc/fstab
UUID=$(/sbin/blkid | grep /dev/mapper/loop1p2 | awk '{ print $2 }' | awk -F'"' '{print $2}')
echo "crypted UUID=$UUID none luks,initramfs" > /etc/crypttab
sed -i "s/root=LABEL=writable/root=\/dev\/mapper\/crypted cryptdevice=UUID=$UUID:crypted/" /boot/cmdline.txt
touch /boot/ssh
echo "CRYPTSETUP=y" >> /etc/cryptsetup-initramfs/conf-hook
patch --no-backup-if-mismatch /usr/share/initramfs-tools/hooks/cryptroot << 'EOF'
--- cryptroot
+++ cryptroot
@@ -33,7 +33,7 @@
         printf '%s\0' "$target" >>"$DESTDIR/cryptroot/targets"
         crypttab_find_entry "$target" || return 1
         crypttab_parse_options --missing-path=warn || return 1
-        crypttab_print_entry
+        printf '%s %s %s %s\n' "$_CRYPTTAB_NAME" "$_CRYPTTAB_SOURCE" "$_CRYPTTAB_KEY" "$_CRYPTTAB_OPTIONS" >&3
     fi
 }
EOF
sed -i 's/^TIMEOUT=.*/TIMEOUT=100/g' /usr/share/cryptsetup/initramfs/bin/cryptroot-unlock
/sbin/mkinitramfs -o /boot/initrd.img "$(ls /lib/modules)"
mv /etc/resolv.conf.bak /etc/resolv.conf
sync
exit
