#!/bin/sh
umount /mnt/chroot/boot
umount /mnt/chroot/sys
umount /mnt/chroot/proc
umount /mnt/chroot/dev/pts
umount /mnt/chroot/dev
umount /mnt/chroot
cryptsetup close crypted
umount /mnt/original
rm -d /mnt/chroot
rm -d /mnt/original
kpartx -d "$PWD/ubuntu-base.img"
kpartx -d "$PWD/ubuntu-target.img"

dmsetup remove_all
losetup -d /dev/loop0
losetup -d /dev/loop1
